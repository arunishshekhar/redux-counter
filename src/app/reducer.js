import { initialState } from "./actions.js"

// reducer
export const reducer = (state = initialState, action) => {
    switch (action.type) {
      case "decrement":
        return {
          ...state,
          [action.uid]: state[action.uid] - 1
        }
      case "increment":
        return {
          ...state,
          [action.uid]: state[action.uid] + 1
        }
  
      case "create-new":
        return {
          ...state,
          [action.uid]: 0,
          allCounterId: state.allCounterId.concat(action.uid)
        }
  
      default:
        return state
    }
  }
  