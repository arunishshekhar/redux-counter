// action
export function decrement(id) {
    return {
        type: "decrement",
        uid: id
    }
}

export function increment(id) {
    return {
        type: "increment",
        uid: id
    }
}

const makeid = () => {
    var text = "";
    var possible = "0123456789";

    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

export function create() {
    return {
        type: "create-new",
        uid: makeid()
    }
}

export const initialState = {
    allCounterId: [],
}
