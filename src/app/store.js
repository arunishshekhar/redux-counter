import { configureStore } from "@reduxjs/toolkit"
import { reducer } from "./reducer.js";

const createStore = configureStore({ reducer: reducer })
export default createStore;
