import React from "react";
import store from '../app/store.js';
import { useSelector, useDispatch } from 'react-redux'
import { increment, decrement, create } from '../app/actions.js'
import "./counter.css"

function Counter() {

    const dispatch = useDispatch()
    const state = useSelector((state) => state)

    return (
        <div className="main-div">
            <div>
                {(store.getState().allCounterId.length !== 0)
                    ? store.getState().allCounterId.map((indi) =>
                        <div key={indi} className="indi-div">
                            <div onClick={() => dispatch(decrement(indi))} className="ctrl">-</div>
                            <div>{state[indi]}</div>
                            <div onClick={() => dispatch(increment(indi))} className="ctrl">+</div>
                        </div>
                    ) :
                    <h1>Add Counter</h1>}
            </div>
            <div>
                <button onClick={() => store.dispatch(create())} className="btn">Create a new Counter</button>
            </div>
        </div>
    )
}

export default Counter